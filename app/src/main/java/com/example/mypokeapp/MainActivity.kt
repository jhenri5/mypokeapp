package com.example.mypokeapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /** Gets called when the user clicks on the Bulbasuar button*/
    fun pickBulbasuar(view: View) {
        val intent = Intent(this, DisplayBulbasuar::class.java)
        startActivity(intent)
    }

    fun pickSquirtle(view: View) {
        val intent = Intent(this, DisplaySquirtle::class.java)
        startActivity(intent)
    }

    /** Gets called when the user clicks on the Charmander button */
    fun pickCharmander(view: View) {
        val intent = Intent(this, DisplayCharmander::class.java)
        startActivity(intent)
    }

}